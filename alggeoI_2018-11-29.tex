\section{Schemes over Schemes}
\begin{definition}[scheme over $S$]
	Let $S$ be a scheme. A \textbf{scheme over $S$} is a scheme $X$ together with a morphism $f \colon X \to S$.
\end{definition}
\begin{proposition}Giving $X \to \Spec R \eqqcolon S$ is equivalent to giving all rings $\MO_X(U)$ the structure of $R$-algebra such that the restriction morphisms are $R$-algebra homomorphisms. 
\end{proposition}
\begin{proof}
	Given $X \to \Spec R$, define $R \to \MO_X(U)$ by
	\[\begin{tikzcd}
		R \arrow{r}[swap]{f^\#(\Spec R)} \arrow[bend left]{rr} & \MO_X(X) \arrow{r}{r_{UX}} \arrow{dr}{r_{VX}} & \MO_X(U) \arrow{d}{r_{VU}} \\
		& & \MO_X(V)
	\end{tikzcd}\]
	By construction, $r_{UV}$ are $R$-algebra homomorphisms. 
	
	Conversly, given $R$-algebra structure on $\MO_X(U)$, we have $R \to \MO_X(X)$. Let $X \to \Spec R$ be the associated morphism via $\Hom_{\Sch}(X,\Spec R) = \Hom_{\Ring}(R,\MO_X(X))$.
\end{proof}
\begin{proposition}
	Given a morphism $f \colon X \to Y$ over $\Spec R$, i.e. a commutative diagram \[\begin{tikzcd}
	X \arrow{rr}{f} \arrow{dr}{a} & & Y \arrow{dl}{a^\prime} \\
	& \Spec R
	\end{tikzcd}\]
	is equivalent to giving a morphism $f \colon X \to Y$ such that $f^\#(U) \colon \MO_Y(U) \to \MO_X(f^{-1}(U))$ is an $R$-algebra homomorphism for all open $U \subset Y$.
\end{proposition}
\begin{proof}
	Take the global regular functions of the diagram above. We get \[a^\#(\Spec R) = f^\#(Y) \circ (a^\prime)^\#(\Spec R) \colon R \to \MO_X(X)\] which implies that $f^\#(Y)$ is an $R$-algebra homomorphism. 
	
	In general, 
	\[\begin{tikzcd}
		\MO_X(X) \arrow{d}{r_{f^{-1}(U)X}} & \MO_Y(Y) \arrow{d}{r_{UY}} \arrow{l}{f^\#(Y)} \\
		\MO_X(f^{-1}(U)) & \MO_Y(U) \arrow{l}{f^\#(U)} \\
		R \arrow[bend left=70]{uu}{a_X^\#} \arrow{u}{a_U^\#} \arrow[equal]{r} & R \arrow{u}{(a_U^\prime)^\#} \arrow[bend right=70,swap]{uu}{(a_Y^\prime)^\#}
	\end{tikzcd}\]
\end{proof}
\begin{definition}[category of schemes over $S$]
	Let $S$ be a scheme. The \textbf{category of schemes over $S$} denoted by $\Sch/S$ is the category with
	\begin{itemize}
		\item objects $(X,X\xrightarrow{a}S)$
		\item morphisms: commutative diagrams 
		\[\begin{tikzcd}
		X \arrow{rr}{f} \arrow{dr}{a} & & Y \arrow{dl}{a^\prime} \\
		& S
		\end{tikzcd}\]
	\end{itemize} 
	 We write $X/S$ for $X \to S$. Also $X/R$ or $\Sch/R$ if $S=\Spec R$.
	 
	 The scheme $S$ is called \textbf{base scheme}.
\end{definition}
\begin{example}
	$\Spec \Z$ is the final object in $\Sch$. Therefore, $\Sch/\Z = \Sch$.
\end{example}
\begin{example}
	There is \textbf{no} morphism $\Spec \R \to \Spec \C$, so we cannot make sense of $\Spec \R$ in $\Sch/\C$. 
\end{example}
\begin{definition}[$T$-valued point]
	Let $T$ and $X$ be schemes over $S$. 
	A \textbf{$T$-valued point of $X$} is a commutative diagram \[\begin{tikzcd}
	T \arrow{rr}{} \arrow{dr}{} & & X \arrow{dl}{} \\
	& S
	\end{tikzcd}\]
	equivalently, a morphism in $\Sch/S$. We write \[X_S(T) = \{ T-\text{valued point of $X$ over $S$} \}=\Hom_{\Sch/S} (T/S,X/S)\]
\end{definition}
\begin{example}
	Let $X=\Spec \C$. Then \[X(\C)=\Hom_{\Sch}(\Spec \C, \Spec \C) = \Hom_{\Ring}(\C,\C) \supset \Gal(\C / \Q)\] Consider $\Spec C$ in $\Sch/\Spec \C$ via $\id \colon \Spec \C \to \Spec \C$. Then, \[X_\C(\C)=\{ \id \}\]
\end{example}
\begin{note}
	If $S$ i clear from the context, we might drop it from notation.
\end{note}
\begin{example}
	Let $k$ be algebraically closed. Let $X$ be a prevariety over $k$. Then, $\MO_X(U)$ is a $k$-algebra and $t(X)$ is naturally a scheme over $k$.
	We have
	\begin{align*}
		t(X)_k(k) = & \Hom_{\Sch/k}(\Spec k,t(X)) \\ = & \setdef{\pid \in t(X)}{k(\pid) \hookrightarrow k} \\ = & \setdef{\pid \in t(X)}{ k(\pid) = k} = X
	\end{align*}
\end{example}
\begin{definition}[base change]
	Let $b \colon S^\prime \to S$ be a morphism and $X/S \in \Sch/S$. 
	Let $X^\prime = S^\prime \times_S X$. 
	We say that $X^\prime$ is \textbf{obtained from $X$ via base extension}. 
\end{definition}
\begin{example}
	Let \[X=\Spec \R[x,y]/(x^2+y^2) \xrightarrow{\qquad a \qquad} \Spec \R = S\] and let $S^\prime = \Spec \C$ where $S^\prime \xrightarrow{b} S$ is given by the natural inclusion.
	We obtain
	\[\begin{tikzcd}
		\Spec \C[x,y]/(x^2+y^2) \arrow{r} \arrow{d} & \Spec \R[x,y]/(x^2+y^2) \arrow{d} \\
		\Spec \C \arrow{r}{i} & \Spec \R
	\end{tikzcd}\]
	as the fiber product. 
\end{example}
\chapter{Properties of Schemes and Morphisms}
Let $X$ be a scheme, $X=(|X|,\MO_X)$ where $|X|$ denotes the underlying topological space. 
\begin{definition}[irreducible, connected, quasi-compact, reduced, integral, locally Noetherian, Noetherian]
	A scheme $X$ is \textbf{irreducible} (\textbf{connected}, \textbf{quasi-compact}) if $|X|$ is irreducible (connected, quasi-compact). 
	
	A scheme $X$ is \textbf{reduced} if $\MO_X(U)$ has no nilpotents for all open $U \subset X$. 
	Equivalently, if $\MO_{X,\pid}$ has no nilpotents for all $\pid \in X$. 
	
	A scheme $X$ is \textbf{integral} if $\MO_X(U)$ is an integral domain for all open $U \subset X$. 
	Equivalently, if $X$ is reduced and irreducible (exercise).
	
	A scheme $X$ is \textbf{locally Noetherian} if there exists a cover $X = \bigcup_{i \in I} U_i$ by affine open subschemes $U_i = \Spec A_i$ such that $A_i$ is Noetherian ($\MO_X(U_i)=A_i$)
	
	A scheme $X$ is \textbf{Noetherian} if $X$ is local Noetherian and quasi-compact. 
\end{definition}
\begin{remark}
	Let $X$ be a scheme and $U \subset X$ be open. Then $(U, \res{\MO_X}{U})$ is a schemes called an \textbf{open subscheme of $X$.}
\end{remark}
\section{Affine Local Properties}
\begin{proposition} \label{LoNo}
	A scheme $X$ is locally Noetherian iff for every open affine subscheme $U=\Spec A \subset X$, the ring $A$ is Noetherian. 
\end{proposition}
\begin{lemma}[affine local] \label{keylemma}
	Let $P$ be a property of affine open subschemes of a scheme $X$ such that 
	\begin{enumerate}
		\item If an affine open subscheme $\Spec A \subset X$ has $P$, then $\Spec A_f$ has $P$.
		\item If $\Spec A = \bigcup_i \Spec A_{f_i}$ and $\Spec A_{f_i}$ has $P$ for all $i$, then $\Spec A$ has $P$.
	\end{enumerate}
	Then if $X=\bigcup_i \Spec A_i$ such that every $\Spec A_i$ has $P$, then every affine open subscheme in $X$ has $P$. We say $P$ is \textbf{affine local}.
\end{lemma}
\begin{proof}
	Let $V = \Spec B \subset X$ be an open affine subscheme which implies $V = \bigcup_i (V \cap \Spec A_i)$. 
	Let $\Spec (A_i)_{g_{ij}}$ be a cover of $V \cap \Spec A_i$. 
	We know that $\Spec (A_i)_{g_{ij}}$ has $P$ since $\Spec A_i$ has $P$.
	Furthermore, $V$ is covered by $\Spec (A_i)_{g_{ij}}$ for all $i,j$. 
	Let $\Spec (B_{h_{ijk}})$ be an affine cover of $\Spec (A_i)_{g_{ij}}$. 
	
	\textbf{Claim.} $\Spec (B_{h_{ijk}})$ is a basic open subset of $\Spec(A_i)_{g_{ij}}$. 
	
	\textit{Proof.} We have 
	\begin{align*}
		\Spec(B_{h_{ijk}}) = D(h_{ijk}) = D(\res{h_{ijk}}{\Spec (A_i)_{g_{ij}}}) = \Spec \left(\res{((A_i)_{g_{ij}})_{h_{ijk}}}{\Spec (A_i)_{g_{ij}}}\right)
	\end{align*}
	which proves the claim.
	
	By the assumptions and the claim, $\Spec(B_{h_{ijk}})$ has $P$. Now, $\Spec B = \bigcup \Spec(B_{h_{ijk}})$ which implies that $\Spec B$ has $P$.
\end{proof}
\begin{proof}[Proof of \Cref{LoNo}]
	We say that $\Spec A$ has $P$ if $A$ is Noetherian. 
	
	We need to prove that $P$ is affine local, in particular
	\begin{enumerate}
		\item If $A$ is Noetherian, then $A_f$ is Noetherian.
		\item If $\Spec A = \bigcup_i \Spec A_{f_i}$ such that each $A_{f_i}$ is Noetherian, then $A$ is Noetherian. 
		
		This is also true. We have $(f_i | i \in I) = A \implies 1 = f_1g_1 + \dots + f_lg_l$ for $g_i \in A$. 
		Let $I_1 \subset I_2 \subset \dots$ be a chain.
		Let $I = \bigcup_j I_j$. 
		If $1 \in I \implies 1 \in I_{j_0}$ for some $j_0$ and we are done. 
		Otherwise, there exists $k \in \{1, \dots , l\}$ such that $f_k \notin I$. But then our chain corresponds to a chain $I_1^\prime \subset I_2^\prime \subset \dots$ in $A_{f_k}$ which stabilizes. \qedhere 
	\end{enumerate}
\end{proof}
\begin{definition}[(locally of) finite type]
	A morphism of schemes $f \colon X \to Y$ is \textbf{locally of finite type} if there exists an open affine cover $S=\bigcup_{i \in I} \Spec B_i$ and open affine cover $f^{-1}(\Spec B_i)=\bigcup_{j \in J_i} \Spec (A_{ij})$ such that $A_{ij}$ is finitely generated over $B_i$.
	
	The morphism $f$ is of \textbf{finite type} if $J_i$ can be chosen to be finite for every $i$.
\end{definition}
\begin{example}~ \vspace{-\topsep}
	\begin{itemize}
		\item $\Spec(k[x_1, \dots , x_n]/(f_1, \dots , f_m)) \to \Spec k$ is of finite type
		\item $\bigsqcup_{i \in I} \Spec k \to \Spec k$ is locally of finite type but not of finite type if $I$ is infinite. 
		\item $\Spec k[x_1, x_2, \dots ] \to \Spec k$ is not locally of finite type. 
	\end{itemize}
\end{example}
\begin{proposition}~ \vspace{- \topsep}
	\begin{enumerate}
		\item $f$ is of finite type $\iff$ $f$ is locally of finite type and quasi-compact (i.e. the preimage of every open affine is quasicompact).
		\item $f$ is locally of finite type $\iff$ for every open $\Spec B \subset Y$ and for every open $\Spec A \subset f^{-1}(\Spec B)$, $A$ is a finitely generated $B$-algebra. 
	\end{enumerate}
\end{proposition}
\begin{proof}~ \vspace{-\topsep} \phantom\qedhere
	\begin{enumerate}
		\item Exercise.
		\item \textbf{Step 1:} We say $\Spec B$ has $P$ if there exists an open cover $f^{-1}(\Spec B)=\bigcup \Spec A_i$ such that each $A_i$ is finitely generated over $B$.
		
		We want to check that $P$ is affine local property (exercise). 
		By \Cref{keylemma}, every open affine has $P$. 
		
		\textbf{Step 2:} Let $\Spec B \subset Y$ be open affine. We say $\Spec A \subset f^{-1}(\Spec B)$ has $P^\prime$ if $A$ is finitely generated $B$-algebra. 
		
		\textbf{Claim.} The property $P^\prime$ is affine local. 
		
		\textit{Proof.} We need to check the two properties.
		\begin{enumerate}
			\item If $A$ is finitely generated over $B$, then so is $A_g=A[1/g]$.
			\item Let $\Spec A = \bigcup \Spec(A_{g_i})$ such that $A_{g_i}$ are finitely generated over $B$. Then $(g_1, \dots , g_n) = A$ which implies $1=g_1h_1+ \dots + g_nh_n$ for some $h_i \in A$. 
			Let $\frac{r_{ij}}{g_i^{n_i}}$ be te generators of $A_{g_i}$ over $B$.
			We claim that $\{r_{ij},g_i,h_i\}$ generate $A$ over $B$.
			Indeed, if $\alpha \in A$, then $\res{\alpha}{A_{ij}}$ is a polynomial in $\frac{r_{ij}}{g_i^{n_i}}$.
			Therefore, there exists $m \ge 1$ such that $g_i^m(\alpha g_i^{n_i}-r_{ij})=0$ in $A$. 
			This implies $\alpha g_i^{n_i+m} = g_i^mr_{ij} \in B[G]$. 
			There exists $N \ge 1$ such that for all $i$, $\alpha g_i^N \in B[G]$. 
			Finally, there exists $\tilde N \ge 1$ such that \[\pushQED{\qed}  \alpha =  1^{\tilde N}a = (\sum g_ih_i)^{\tilde N} \alpha \in B[G] \, \qedhere \popQED .\]
		\end{enumerate}
	\end{enumerate}
\end{proof}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
